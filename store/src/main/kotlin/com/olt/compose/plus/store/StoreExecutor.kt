package com.olt.compose.plus.store

interface StoreExecutor<in Intent: Any, in State: Any, out Msg: Any> {

    sealed class Result<out Msg: Any> {
        data class Completable<Msg: Any>(
            val msg: Msg? = null,
            val completable: com.badoo.reaktive.completable.Completable
        ) : Result<Msg>()

        data class Maybe<Msg: Any>(
            val msg: Msg? = null,
            val maybe: com.badoo.reaktive.maybe.Maybe<*>
        ) : Result<Msg>()

        data class Observable<Msg: Any>(
            val msg: Msg? = null,
            val observable: com.badoo.reaktive.observable.Observable<*>
        ) : Result<Msg>()

        data class Single<Msg: Any>(
            val msg: Msg? = null,
            val single: com.badoo.reaktive.single.Single<*>
        ) : Result<Msg>()

        data class Message<Msg: Any>(val msg: Msg) : Result<Msg>()
    }

    fun executeIntent(intent: Intent, getState: () -> State): Result<Msg>

    sealed class Action<out Msg: Any> {
        data class Maybe<Msg: Any>(val maybe: () -> com.badoo.reaktive.maybe.Maybe<Msg>) : Action<Msg>()

        data class Observable<Msg: Any>(val observable: () -> com.badoo.reaktive.observable.Observable<Msg>) : Action<Msg>()
    }

    val actions: List<Action<Msg>>
}