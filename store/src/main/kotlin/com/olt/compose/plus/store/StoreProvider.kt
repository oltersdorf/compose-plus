package com.olt.compose.plus.store

import com.arkivanov.mvikotlin.core.store.SimpleBootstrapper
import com.arkivanov.mvikotlin.core.store.Store
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.arkivanov.mvikotlin.extensions.reaktive.ReaktiveExecutor
import com.badoo.reaktive.maybe.observeOn
import com.badoo.reaktive.observable.observeOn
import com.badoo.reaktive.scheduler.mainScheduler

abstract class StoreProvider<Intent: Any, State: Any, Msg: Any>(
    private val name: String,
    private val storeFactory: StoreFactory,
    private val storeExecutor: () -> StoreExecutor<Intent, State, Msg>
) {

    internal fun provide(): Store<Intent, State, Nothing> =
        object : Store<Intent, State, Nothing> by storeFactory.create(
            name = name,
            initialState = initialState(),
            bootstrapper = SimpleBootstrapper(Unit),
            executorFactory = { ExecutorImpl(storeExecutor()) },
            reducer = { msg -> reduce(msg) }
        ) {}

    protected abstract fun initialState(): State

    private inner class ExecutorImpl<Intent: Any, State: Any, Msg: Any>(
        private val storeExecutor: StoreExecutor<Intent, State, Msg>
    ) : ReaktiveExecutor<Intent, Unit, State, Msg, Nothing>() {

        override fun executeAction(action: Unit, getState: () -> State) {
            storeExecutor.actions.forEach {
                when (it) {
                    is StoreExecutor.Action.Maybe -> it.maybe().observeOn(mainScheduler).subscribeScoped(onSuccess = ::dispatch)
                    is StoreExecutor.Action.Observable -> it.observable().observeOn(mainScheduler).subscribeScoped(onNext = ::dispatch)
                }
            }
        }

        override fun executeIntent(intent: Intent, getState: () -> State) {
            when (val result = storeExecutor.executeIntent(intent = intent, getState = getState)) {
                is StoreExecutor.Result.Message -> dispatch(message = result.msg)
                is StoreExecutor.Result.Completable -> {
                    result.msg?.let { dispatch(it) }
                    result.completable.subscribeScoped()
                }
                is StoreExecutor.Result.Maybe -> {
                    result.msg?.let { dispatch(it) }
                    result.maybe.subscribeScoped()
                }
                is StoreExecutor.Result.Observable -> {
                    result.msg?.let { dispatch(it) }
                    result.observable.subscribeScoped()
                }
                is StoreExecutor.Result.Single -> {
                    result.msg?.let { dispatch(it) }
                    result.single.subscribeScoped()
                }
            }
        }
    }

    protected abstract fun State.reduce(msg: Msg): State
}