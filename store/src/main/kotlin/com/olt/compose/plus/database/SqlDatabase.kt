package com.olt.compose.plus.database

import com.badoo.reaktive.base.setCancellable
import com.badoo.reaktive.completable.Completable
import com.badoo.reaktive.maybe.Maybe
import com.badoo.reaktive.observable.*
import com.badoo.reaktive.scheduler.ioScheduler
import com.badoo.reaktive.single.*
import com.squareup.sqldelight.Query
import com.squareup.sqldelight.Transacter
import com.squareup.sqldelight.db.SqlDriver

abstract class SqlDatabase<Database: Transacter>(private val database: Single<Database>) {

    constructor(driver: SqlDriver, mapper: (SqlDriver) -> Database) : this(singleOf(driver).map(mapper = mapper))

    protected fun <Query: Transacter> databaseQuery(mapper: (Database) -> Query): Single<Query> =
        database.map(mapper = mapper)
            .asObservable()
            .replay()
            .autoConnect()
            .firstOrError()

    protected fun <T : Any, Q: Transacter> queryObservable(query: Single<Q>, queryMapper: (Q) -> Query<T>): Observable<List<T>> =
        query
            .observeOn(ioScheduler)
            .map(queryMapper)
            .observe { it.executeAsList() }

    protected fun <T : Any, Q: Transacter> queryMaybe(query: Single<Q>, queryMapper: (Q) -> Query<T>): Maybe<T> =
        query
            .observeOn(ioScheduler)
            .map(queryMapper)
            .mapNotNull { it.executeAsOneOrNull() }

    protected fun <Q: Transacter> execute(query: Single<Q>, queryExecution: (Q) -> Unit): Completable =
        query
            .observeOn(ioScheduler)
            .doOnBeforeSuccess(queryExecution)
            .asCompletable()

    private fun <T : Any, R> Single<Query<T>>.observe(get: (Query<T>) -> R): Observable<R> =
        flatMapObservable { it.observed() }
            .observeOn(ioScheduler)
            .map(get)

    private fun <T : Any> Query<T>.observed(): Observable<Query<T>> =
        observable { emitter ->
            val listener =
                object : Query.Listener {
                    override fun queryResultsChanged() {
                        emitter.onNext(this@observed)
                    }
                }

            emitter.onNext(this@observed)
            addListener(listener)
            emitter.setCancellable { removeListener(listener) }
        }
}