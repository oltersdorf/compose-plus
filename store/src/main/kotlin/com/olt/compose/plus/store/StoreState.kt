package com.olt.compose.plus.store

interface StoreState<out Model: Any> {
    fun toModel(): Model
}