package com.olt.compose.plus.store

import com.arkivanov.decompose.value.Value

interface UiInterface<Model: Any> {

    val models: Value<Model>
}