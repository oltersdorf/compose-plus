package com.olt.compose.plus.store

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.value.Value
import com.arkivanov.decompose.value.operator.map
import com.arkivanov.mvikotlin.core.instancekeeper.getStore
import com.arkivanov.mvikotlin.core.store.Store
import com.olt.compose.plus.utils.asValue

interface Component<Model: Any, Intent: Any, State: Any> : UiInterface<Model>, ComponentContext {
    val store: Store<Intent, State, Nothing>

    companion object {
        fun <Model: Any, Intent: Any, State: StoreState<Model>, Msg: Any> create(
            componentContext: ComponentContext,
            storeProvider: StoreProvider<Intent, State, Msg>
        ): Component<Model, Intent, State> =
            object : Component<Model, Intent, State>, ComponentContext by componentContext {

                override val store by lazy {
                    instanceKeeper.getStore {
                        storeProvider.provide()
                    }
                }

                override val models: Value<Model> = store.asValue().map { it.toModel() }
            }
    }
}