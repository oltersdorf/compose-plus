plugins {
    id("setup")
}

dependencies {
    implementation(Deps.ArkIvanov.MVIKotlin.mvikotlin)
    implementation(Deps.ArkIvanov.MVIKotlin.rx)
    implementation(Deps.ArkIvanov.MVIKotlin.mvikotlinExtensionsReaktive)
    implementation(Deps.ArkIvanov.Decompose.decompose)
    implementation(Deps.Badoo.Reaktive.reaktive)
    implementation(Deps.Squareup.SQLDelight.sqliteDriver)
}