plugins {
    id("setup")
}

dependencies {
    implementation(project(":store"))
    implementation(Deps.ArkIvanov.MVIKotlin.mvikotlin)
    implementation(Deps.ArkIvanov.Decompose.decompose)
    implementation(Deps.Badoo.Reaktive.reaktive)
}