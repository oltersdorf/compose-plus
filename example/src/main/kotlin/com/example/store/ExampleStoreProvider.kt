package com.example.store

import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.olt.compose.plus.store.StoreProvider

internal class ExampleStoreProvider(
    storeFactory: StoreFactory
) : StoreProvider<Intent, State, Msg>(
    name = "ExampleStore",
    storeFactory = storeFactory,
    storeExecutor = ::ExampleStoreExecutor
) {
    override fun initialState(): State = State()

    override fun State.reduce(msg: Msg): State =
        when (msg) {
            is Msg.DataLoaded -> copy(data = msg.data)
            is Msg.ModelChanged -> copy(data = msg.newData)
        }
}