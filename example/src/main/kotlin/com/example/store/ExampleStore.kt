package com.example.store

import com.example.Example.Model
import com.olt.compose.plus.store.StoreState

internal sealed class Intent {
    data class ChangeModel(val newData: String) : Intent()
}

internal data class State(
    val data: String = ""
) : StoreState<Model> {

    override fun toModel(): Model =
        Model(data = data)
}

internal sealed class Msg {
    data class ModelChanged(val newData: String) : Msg()
    data class DataLoaded(val data: String) : Msg()
}