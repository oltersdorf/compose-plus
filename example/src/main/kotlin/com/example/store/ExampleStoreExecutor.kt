package com.example.store

import com.badoo.reaktive.completable.completable
import com.badoo.reaktive.observable.toObservable
import com.olt.compose.plus.store.StoreExecutor

internal class ExampleStoreExecutor : StoreExecutor<Intent, State, Msg> {

    override fun executeIntent(intent: Intent, getState: () -> State): StoreExecutor.Result<Msg> =
        when (intent) {
            is Intent.ChangeModel -> StoreExecutor.Result.Completable(
                msg = Msg.ModelChanged(newData = intent.newData),
                completable = completable {
                    //For example: Push change to database
                }
            )
        }

    override val actions: List<StoreExecutor.Action<Msg>> =
        listOf(StoreExecutor.Action.Observable(observable = { Msg.DataLoaded(data = "").toObservable() }))
}