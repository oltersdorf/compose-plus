package com.example

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.mvikotlin.core.store.StoreFactory
import com.example.store.ExampleStoreProvider
import com.example.store.Intent
import com.example.store.State
import com.olt.compose.plus.store.Component

class ExampleComponent private constructor(
    componentContext: ComponentContext,
    storeFactory: StoreFactory
) : Example, Component<Example.Model, Intent, State> by Component.create(componentContext, ExampleStoreProvider(storeFactory)) {

    override fun changeModel(differentData: String) {
        store.accept(Intent.ChangeModel(differentData))
    }
}