package com.example

import com.example.Example.Model
import com.olt.compose.plus.store.UiInterface

interface Example: UiInterface<Model> {

    data class Model(
        val data: String
    )

    fun changeModel(differentData: String)
}