import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

kotlin {
    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }
}